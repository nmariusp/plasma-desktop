# Lithuanian translations for plasma-desktop package.
# Copyright (C) 2023 This file is copyright:
# This file is distributed under the same license as the plasma-desktop package.
# Automatically generated, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-12-12 00:48+0000\n"
"PO-Revision-Date: 2022-12-12 00:48+0000\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"Language: lt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n"
"%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);\n"

#. i18n: ectx: label, entry (colorScheme), group (General)
#: landingpage_kdeglobalssettings.kcfg:9
#, kde-format
msgid "Color scheme name"
msgstr ""

#. i18n: ectx: label, entry (singleClick), group (KDE)
#: landingpage_kdeglobalssettings.kcfg:15
#, kde-format
msgid "Single click to open files"
msgstr ""

#. i18n: ectx: label, entry (lookAndFeelPackage), group (KDE)
#: landingpage_kdeglobalssettings.kcfg:19
#, kde-format
msgid "Global Look and Feel package"
msgstr ""

#. i18n: ectx: label, entry (defaultLightLookAndFeel), group (KDE)
#. i18n: ectx: label, entry (defaultDarkLookAndFeel), group (KDE)
#: landingpage_kdeglobalssettings.kcfg:23
#: landingpage_kdeglobalssettings.kcfg:27
#, kde-format
msgid "Global Look and Feel package, alternate"
msgstr ""

#. i18n: ectx: label, entry (animationDurationFactor), group (KDE)
#: landingpage_kdeglobalssettings.kcfg:31
#, kde-format
msgid "Animation speed"
msgstr ""

#: package/contents/ui/main.qml:27
#, kde-format
msgid "Theme:"
msgstr ""

#: package/contents/ui/main.qml:68
#, kde-format
msgid "Animation speed:"
msgstr ""

#: package/contents/ui/main.qml:91
#, kde-format
msgctxt "Animation speed"
msgid "Slow"
msgstr ""

#: package/contents/ui/main.qml:97
#, kde-format
msgctxt "Animation speed"
msgid "Instant"
msgstr ""

#: package/contents/ui/main.qml:111
#, kde-format
msgid "Change Wallpaper…"
msgstr ""

#: package/contents/ui/main.qml:118
#, kde-format
msgid "More Appearance Settings…"
msgstr ""

#: package/contents/ui/main.qml:133
#, kde-format
msgctxt ""
"part of a sentence: 'Clicking files or folders [opens them/selects them]'"
msgid "Clicking files or folders:"
msgstr ""

#: package/contents/ui/main.qml:134
#, kde-format
msgctxt "part of a sentence: 'Clicking files or folders opens them'"
msgid "Opens them"
msgstr ""

#: package/contents/ui/main.qml:147
#, kde-format
msgid "Select by clicking on item's selection marker"
msgstr ""

#: package/contents/ui/main.qml:158
#, kde-format
msgctxt "part of a sentence: 'Clicking files or folders selects them'"
msgid "Selects them"
msgstr ""

#: package/contents/ui/main.qml:172
#, kde-format
msgid "Open by double-clicking instead"
msgstr ""

#: package/contents/ui/main.qml:184
#, kde-format
msgid "More Behavior Settings…"
msgstr ""

#: package/contents/ui/main.qml:196
#, kde-format
msgid "Most Used Pages:"
msgstr ""
