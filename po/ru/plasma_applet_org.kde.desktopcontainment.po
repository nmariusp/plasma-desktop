# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Leonid Kanter <leon@asplinux.ru>, 2008.
# Артём Середа <overmind88@gmail.com>, 2008.
# Nick Shaforostoff <shafff@ukr.net>, 2008, 2009.
# Andrey Cherepanov <skull@kde.ru>, 2009.
# Alexander Potashev <aspotashev@gmail.com>, 2010, 2012, 2015, 2016, 2017, 2018, 2019, 2020.
# Yuri Efremov <yur.arh@gmail.com>, 2011, 2012, 2013.
# Alexander Lakhin <exclusion@gmail.com>, 2013.
# Alexander Yavorsky <kekcuha@gmail.com>, 2018, 2020, 2021, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-02-05 00:59+0000\n"
"PO-Revision-Date: 2023-01-22 16:37+0300\n"
"Last-Translator: Alexander Yavorsky <kekcuha@gmail.com>\n"
"Language-Team: Russian <kde-russian@lists.kde.ru>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 21.08.3\n"

#: package/contents/config/config.qml:16
#, kde-format
msgid "Location"
msgstr "Путь"

#: package/contents/config/config.qml:23
#: package/contents/ui/FolderViewLayer.qml:429
#, kde-format
msgid "Icons"
msgstr "Значки"

#: package/contents/config/config.qml:30
#, kde-format
msgid "Filter"
msgstr "Фильтр"

#: package/contents/ui/BackButtonItem.qml:103
#, kde-format
msgid "Back"
msgstr "Назад"

#: package/contents/ui/ConfigFilter.qml:64
#, kde-format
msgid "Files:"
msgstr "Файлы:"

#: package/contents/ui/ConfigFilter.qml:65
#, kde-format
msgid "Show all"
msgstr "Показывать все"

#: package/contents/ui/ConfigFilter.qml:65
#, kde-format
msgid "Show matching"
msgstr "Показывать файлы по шаблону"

#: package/contents/ui/ConfigFilter.qml:65
#, kde-format
msgid "Hide matching"
msgstr "Скрывать файлы по шаблону"

#: package/contents/ui/ConfigFilter.qml:70
#, kde-format
msgid "File name pattern:"
msgstr "Шаблон имён файлов:"

#: package/contents/ui/ConfigFilter.qml:77
#, kde-format
msgid "File types:"
msgstr "Типы файлов:"

#: package/contents/ui/ConfigFilter.qml:83
#, kde-format
msgid "Show hidden files:"
msgstr "Показывать скрытые файлы:"

#: package/contents/ui/ConfigFilter.qml:179
#, kde-format
msgid "File type"
msgstr "Тип файла"

#: package/contents/ui/ConfigFilter.qml:188
#, kde-format
msgid "Description"
msgstr "Описание"

#: package/contents/ui/ConfigFilter.qml:201
#, kde-format
msgid "Select All"
msgstr "Выбрать все"

#: package/contents/ui/ConfigFilter.qml:211
#, kde-format
msgid "Deselect All"
msgstr "Отменить выбор"

#: package/contents/ui/ConfigIcons.qml:63
#, kde-format
msgid "Panel button:"
msgstr "Кнопка на панели:"

#: package/contents/ui/ConfigIcons.qml:69
#, kde-format
msgid "Use a custom icon"
msgstr "Использовать нестандартный значок"

#: package/contents/ui/ConfigIcons.qml:102
#, kde-format
msgctxt "@item:inmenu Open icon chooser dialog"
msgid "Choose…"
msgstr "Выбрать..."

#: package/contents/ui/ConfigIcons.qml:108
#, kde-format
msgctxt "@item:inmenu Reset icon to default"
msgid "Clear Icon"
msgstr "Вернуть стандартный значок"

#: package/contents/ui/ConfigIcons.qml:128
#, kde-format
msgid "Arrangement:"
msgstr "Расположение:"

#: package/contents/ui/ConfigIcons.qml:132
#, kde-format
msgctxt "@item:inlistbox arrangement of icons"
msgid "Left to Right"
msgstr "Слева направо"

#: package/contents/ui/ConfigIcons.qml:133
#, kde-format
msgctxt "@item:inlistbox arrangement of icons"
msgid "Right to Left"
msgstr "Справа налево"

#: package/contents/ui/ConfigIcons.qml:134
#, kde-format
msgctxt "@item:inlistbox arrangement of icons"
msgid "Top to Bottom"
msgstr "Сверху вниз"

#: package/contents/ui/ConfigIcons.qml:143
#, kde-format
msgctxt "@item:inlistbox alignment of icons"
msgid "Align left"
msgstr "Выровнять по левому краю"

#: package/contents/ui/ConfigIcons.qml:144
#, kde-format
msgctxt "@item:inlistbox alignment of icons"
msgid "Align right"
msgstr "Выровнять по правому краю"

#: package/contents/ui/ConfigIcons.qml:159
#, kde-format
msgid "Lock in place"
msgstr "Зафиксировать значки"

#: package/contents/ui/ConfigIcons.qml:173
#, kde-format
msgid "Sorting:"
msgstr "Сортировка:"

#: package/contents/ui/ConfigIcons.qml:181
#, kde-format
msgctxt "@item:inlistbox sort icons manually"
msgid "Manual"
msgstr "Вручную"

#: package/contents/ui/ConfigIcons.qml:182
#, kde-format
msgctxt "@item:inlistbox sort icons by name"
msgid "Name"
msgstr "По имени"

#: package/contents/ui/ConfigIcons.qml:183
#, kde-format
msgctxt "@item:inlistbox sort icons by size"
msgid "Size"
msgstr "По размеру"

#: package/contents/ui/ConfigIcons.qml:184
#, kde-format
msgctxt "@item:inlistbox sort icons by file type"
msgid "Type"
msgstr "По типу"

#: package/contents/ui/ConfigIcons.qml:185
#, kde-format
msgctxt "@item:inlistbox sort icons by date"
msgid "Date"
msgstr "По дате"

#: package/contents/ui/ConfigIcons.qml:196
#, kde-format
msgctxt "@option:check sort icons in descending order"
msgid "Descending"
msgstr "По убыванию"

#: package/contents/ui/ConfigIcons.qml:204
#, kde-format
msgctxt "@option:check sort icons with folders first"
msgid "Folders first"
msgstr "Папки в начале"

#: package/contents/ui/ConfigIcons.qml:218
#, kde-format
msgctxt "whether to use icon or list view"
msgid "View mode:"
msgstr "Режим просмотра:"

#: package/contents/ui/ConfigIcons.qml:220
#, kde-format
msgctxt "@item:inlistbox show icons in a list"
msgid "List"
msgstr "Список"

#: package/contents/ui/ConfigIcons.qml:221
#, kde-format
msgctxt "@item:inlistbox show icons in a grid"
msgid "Grid"
msgstr "Сетка"

#: package/contents/ui/ConfigIcons.qml:232
#, kde-format
msgid "Icon size:"
msgstr "Размер значков:"

#: package/contents/ui/ConfigIcons.qml:247
#, kde-format
msgctxt "@label:slider smallest icon size"
msgid "Small"
msgstr "Маленькие"

#: package/contents/ui/ConfigIcons.qml:256
#, kde-format
msgctxt "@label:slider largest icon size"
msgid "Large"
msgstr "Большие"

#: package/contents/ui/ConfigIcons.qml:265
#, kde-format
msgid "Label width:"
msgstr "Ширина надписей:"

#: package/contents/ui/ConfigIcons.qml:268
#, kde-format
msgctxt "@item:inlistbox how long a text label should be"
msgid "Narrow"
msgstr "Узкие"

#: package/contents/ui/ConfigIcons.qml:269
#, kde-format
msgctxt "@item:inlistbox how long a text label should be"
msgid "Medium"
msgstr "Средние"

#: package/contents/ui/ConfigIcons.qml:270
#, kde-format
msgctxt "@item:inlistbox how long a text label should be"
msgid "Wide"
msgstr "Широкие"

#: package/contents/ui/ConfigIcons.qml:278
#, kde-format
msgid "Text lines:"
msgstr "Число строк текста:"

#: package/contents/ui/ConfigIcons.qml:294
#, kde-format
msgid "When hovering over icons:"
msgstr "При наведении указателя мыши на значки:"

#: package/contents/ui/ConfigIcons.qml:296
#, kde-format
msgid "Show tooltips"
msgstr "Показывать всплывающие подсказки"

#: package/contents/ui/ConfigIcons.qml:303
#, kde-format
msgid "Show selection markers"
msgstr "Показывать переключатели выделения"

#: package/contents/ui/ConfigIcons.qml:310
#, kde-format
msgid "Show folder preview popups"
msgstr "Показывать предварительный просмотр содержимого папок"

#: package/contents/ui/ConfigIcons.qml:320
#, kde-format
msgid "Rename:"
msgstr "Переименование:"

#: package/contents/ui/ConfigIcons.qml:324
#, kde-format
msgid "Rename inline by clicking selected item's text"
msgstr "Переименовывать щелчком по тексту выделенного объекта"

#: package/contents/ui/ConfigIcons.qml:335
#, kde-format
msgid "Previews:"
msgstr "Миниатюры:"

#: package/contents/ui/ConfigIcons.qml:337
#, kde-format
msgid "Show preview thumbnails"
msgstr "Использовать миниатюры предварительного просмотра"

#: package/contents/ui/ConfigIcons.qml:345
#, kde-format
msgid "Configure Preview Plugins…"
msgstr "Настроить модули предварительного просмотра..."

#: package/contents/ui/ConfigLocation.qml:81
#, kde-format
msgid "Show:"
msgstr "Показывать:"

#: package/contents/ui/ConfigLocation.qml:83
#, kde-format
msgid "Desktop folder"
msgstr "Папку «Рабочий стол»"

#: package/contents/ui/ConfigLocation.qml:90
#, kde-format
msgid "Files linked to the current activity"
msgstr "Файлы, связанные с текущей комнатой"

#: package/contents/ui/ConfigLocation.qml:97
#, kde-format
msgid "Places panel item:"
msgstr "Точку входа:"

#: package/contents/ui/ConfigLocation.qml:130
#, kde-format
msgid "Custom location:"
msgstr "Следующую папку:"

#: package/contents/ui/ConfigLocation.qml:138
#, kde-format
msgid "Type path or URL…"
msgstr "Путь к папке или адрес URL..."

#: package/contents/ui/ConfigLocation.qml:181
#, kde-format
msgid "Title:"
msgstr "Заголовок:"

#: package/contents/ui/ConfigLocation.qml:183
#, kde-format
msgid "None"
msgstr "Нет"

#: package/contents/ui/ConfigLocation.qml:183
#, kde-format
msgid "Default"
msgstr "По умолчанию"

#: package/contents/ui/ConfigLocation.qml:183
#, kde-format
msgid "Full path"
msgstr "Полный путь"

#: package/contents/ui/ConfigLocation.qml:183
#, kde-format
msgid "Custom title"
msgstr "Свой заголовок"

#: package/contents/ui/ConfigLocation.qml:198
#, kde-format
msgid "Enter custom title…"
msgstr "Введите свой заголовок для папки"

#: package/contents/ui/ConfigOverlay.qml:91
#, kde-format
msgid "Rotate"
msgstr "Повернуть"

#: package/contents/ui/ConfigOverlay.qml:181
#, kde-format
msgid "Open Externally"
msgstr "Открывать во внешней программе"

#: package/contents/ui/ConfigOverlay.qml:193
#, kde-format
msgid "Hide Background"
msgstr "Скрывать фон"

#: package/contents/ui/ConfigOverlay.qml:193
#, kde-format
msgid "Show Background"
msgstr "Отображать фон"

#: package/contents/ui/ConfigOverlay.qml:241
#, kde-format
msgid "Remove"
msgstr "Удалить"

#: package/contents/ui/FolderItemPreviewPluginsDialog.qml:19
#, kde-format
msgid "Preview Plugins"
msgstr "Модули предварительного просмотра"

#: package/contents/ui/main.qml:356
#, kde-format
msgid "Configure Desktop and Wallpaper…"
msgstr "Настроить рабочий стол и обои..."

#: plugins/folder/directorypicker.cpp:32
#, kde-format
msgid "Select Folder"
msgstr "Выбор папки"

#: plugins/folder/foldermodel.cpp:468
#, kde-format
msgid "&Refresh Desktop"
msgstr "&Обновить рабочий стол"

#: plugins/folder/foldermodel.cpp:468 plugins/folder/foldermodel.cpp:1626
#, kde-format
msgid "&Refresh View"
msgstr "О&бновить вид"

#: plugins/folder/foldermodel.cpp:1635
#, kde-format
msgid "&Empty Trash"
msgstr "О&чистить корзину"

#: plugins/folder/foldermodel.cpp:1638
#, kde-format
msgctxt "Restore from trash"
msgid "Restore"
msgstr "Восстановить"

#: plugins/folder/foldermodel.cpp:1641
#, kde-format
msgid "&Open"
msgstr "&Открыть"

#: plugins/folder/foldermodel.cpp:1758
#, kde-format
msgid "&Paste"
msgstr "Вст&авить"

#: plugins/folder/foldermodel.cpp:1875
#, kde-format
msgid "&Properties"
msgstr "&Свойства"

#: plugins/folder/viewpropertiesmenu.cpp:21
#, kde-format
msgid "Sort By"
msgstr "Сортировать"

#: plugins/folder/viewpropertiesmenu.cpp:24
#, kde-format
msgctxt "@item:inmenu Sort icons manually"
msgid "Unsorted"
msgstr "Без сортировки"

#: plugins/folder/viewpropertiesmenu.cpp:28
#, kde-format
msgctxt "@item:inmenu Sort icons by name"
msgid "Name"
msgstr "По имени"

#: plugins/folder/viewpropertiesmenu.cpp:32
#, kde-format
msgctxt "@item:inmenu Sort icons by size"
msgid "Size"
msgstr "По размеру"

#: plugins/folder/viewpropertiesmenu.cpp:36
#, kde-format
msgctxt "@item:inmenu Sort icons by file type"
msgid "Type"
msgstr "По типу"

#: plugins/folder/viewpropertiesmenu.cpp:40
#, kde-format
msgctxt "@item:inmenu Sort icons by date"
msgid "Date"
msgstr "По дате"

#: plugins/folder/viewpropertiesmenu.cpp:45
#, kde-format
msgctxt "@item:inmenu Sort icons in descending order"
msgid "Descending"
msgstr "По убыванию"

#: plugins/folder/viewpropertiesmenu.cpp:47
#, kde-format
msgctxt "@item:inmenu Sort icons with folders first"
msgid "Folders First"
msgstr "Папки в начале"

#: plugins/folder/viewpropertiesmenu.cpp:50
#, kde-format
msgid "Icon Size"
msgstr "Размер значков"

#: plugins/folder/viewpropertiesmenu.cpp:53
#, kde-format
msgctxt "@item:inmenu size of the icons"
msgid "Tiny"
msgstr "Крошечные"

#: plugins/folder/viewpropertiesmenu.cpp:54
#, kde-format
msgctxt "@item:inmenu size of the icons"
msgid "Very Small"
msgstr "Очень маленькие"

#: plugins/folder/viewpropertiesmenu.cpp:55
#, kde-format
msgctxt "@item:inmenu size of the icons"
msgid "Small"
msgstr "Маленькие"

#: plugins/folder/viewpropertiesmenu.cpp:56
#, kde-format
msgctxt "@item:inmenu size of the icons"
msgid "Small-Medium"
msgstr "Меньше средних"

#: plugins/folder/viewpropertiesmenu.cpp:57
#, kde-format
msgctxt "@item:inmenu size of the icons"
msgid "Medium"
msgstr "Средние"

#: plugins/folder/viewpropertiesmenu.cpp:58
#, kde-format
msgctxt "@item:inmenu size of the icons"
msgid "Large"
msgstr "Большие"

#: plugins/folder/viewpropertiesmenu.cpp:59
#, kde-format
msgctxt "@item:inmenu size of the icons"
msgid "Huge"
msgstr "Огромные"

#: plugins/folder/viewpropertiesmenu.cpp:67
#, kde-format
msgctxt "@item:inmenu arrangement of icons"
msgid "Arrange"
msgstr "Упорядочить"

#: plugins/folder/viewpropertiesmenu.cpp:71
#, kde-format
msgctxt "@item:inmenu arrangement of icons"
msgid "Left to Right"
msgstr "Слева направо"

#: plugins/folder/viewpropertiesmenu.cpp:72
#, kde-format
msgctxt "@item:inmenu arrangement of icons"
msgid "Right to Left"
msgstr "Справа налево"

#: plugins/folder/viewpropertiesmenu.cpp:76
#, kde-format
msgctxt "@item:inmenu arrangement of icons"
msgid "Top to Bottom"
msgstr "Сверху вниз"

#: plugins/folder/viewpropertiesmenu.cpp:81
#, kde-format
msgid "Align"
msgstr "Выровнять"

#: plugins/folder/viewpropertiesmenu.cpp:84
#, kde-format
msgctxt "@item:inmenu alignment of icons"
msgid "Left"
msgstr "По левой стороне"

#: plugins/folder/viewpropertiesmenu.cpp:88
#, kde-format
msgctxt "@item:inmenu alignment of icons"
msgid "Right"
msgstr "По правой стороне"

#: plugins/folder/viewpropertiesmenu.cpp:93
#, kde-format
msgid "Show Previews"
msgstr "Показывать миниатюры"

#: plugins/folder/viewpropertiesmenu.cpp:97
#, kde-format
msgctxt "@item:inmenu lock icon positions in place"
msgid "Locked"
msgstr "Зафиксировать значки"

#~ msgid "OK"
#~ msgstr "&ОК"

#~ msgid "Cancel"
#~ msgstr "О&тмена"

#~ msgctxt "@item:inlistbox arrangement of icons"
#~ msgid "Rows"
#~ msgstr "По строкам"

#~ msgctxt "@item:inlistbox arrangement of icons"
#~ msgid "Columns"
#~ msgstr "По столбцам"

#~ msgctxt "@item:inmenu arrangement of icons"
#~ msgid "Rows"
#~ msgstr "По строкам"

#~ msgctxt "@item:inmenu arrangement of icons"
#~ msgid "Columns"
#~ msgstr "По столбцам"

#~ msgid "Features:"
#~ msgstr "Возможности:"

#~ msgid "Search…"
#~ msgstr "Поиск..."

#~ msgid "&Create Folder"
#~ msgstr "Создать &папку"

#~ msgid "Tweaks"
#~ msgstr "Поправки"

#~ msgid ""
#~ "With the Desktop Toolbox hidden, right-click on the desktop and choose "
#~ "'Configure Desktop...' to access this configuration window in the future"
#~ msgstr ""
#~ "Когда кнопка инструментов скрыта, это окно настройки можно открыть, нажав "
#~ "на рабочем столе правой кнопкой мыши и выбрав «Настроить рабочий стол...»."

#~ msgid "Show the desktop toolbox"
#~ msgstr "Показывать кнопку инструментов Plasma"

#~ msgid "Press and hold widgets to move them and reveal their handles"
#~ msgstr ""
#~ "Удерживание левой кнопки мыши на виджете позволяет двигать его и "
#~ "показывает кнопки виджета"

#~ msgid "Widgets unlocked"
#~ msgstr "Виджеты разблокированы"

#~ msgid ""
#~ "You can press and hold widgets to move them and reveal their handles."
#~ msgstr ""
#~ "Нажмите на виджете и подержите кнопку мыши, чтобы начать перемещение или "
#~ "увидеть кнопки виджета."

#~ msgid "Got it"
#~ msgstr "Закрыть"

#~ msgid "Resize"
#~ msgstr "Изменить размер"

#~ msgid "Size:"
#~ msgstr "Размер:"

#~ msgid "Desktop Layout"
#~ msgstr "Вид рабочего стола"

#~ msgid "Widget Handling"
#~ msgstr "Управление виджетами"

#~ msgid "Arrange in"
#~ msgstr "Упорядочить"

#~ msgid "Sort by"
#~ msgstr "Сортировать"

#~ msgid "Appearance:"
#~ msgstr "Внешний вид:"

#~ msgid "Location:"
#~ msgstr "Путь:"

#~ msgid "Show the Desktop folder"
#~ msgstr "Показать папку рабочего стола"

#~ msgid "Specify a folder:"
#~ msgstr "Показать указанную папку:"

#~ msgid "&Reload"
#~ msgstr "О&бновить"

#~ msgid "&Move to Trash"
#~ msgstr "У&далить в корзину"

#~ msgid "&Delete"
#~ msgstr "&Удалить"

#~ msgid "Align:"
#~ msgstr "Выровнять:"

#~ msgid "Sorting"
#~ msgstr "Сортировка"

# Dummy list view item that you can click to go to parent directory. --aspotashev
#~ msgid "Up"
#~ msgstr "Вверх"

#~ msgid ""
#~ "Tweaks are experimental options that may become defaults depending on "
#~ "your feedback."
#~ msgstr ""
#~ "Экспериментальные параметры, определённые значения которых могут стать "
#~ "стандартными в зависимости от отзывов пользователей."

#~ msgid "Show Original Directory"
#~ msgstr "Открыть папку назначения"

#~ msgid "Show Original File"
#~ msgstr "Открыть файл назначения"

#~ msgid "&Configure Trash Bin"
#~ msgstr "Настроить &корзину..."

#~ msgid "&Bookmark This Page"
#~ msgstr "Добавить &закладку на эту страницу"

#~ msgid "&Bookmark This Location"
#~ msgstr "Добавить &закладку на этот адрес"

#~ msgid "&Bookmark This Folder"
#~ msgstr "Добавить &закладку на эту папку"

#~ msgid "&Bookmark This Link"
#~ msgstr "Добавить &закладку на эту ссылку"

#~ msgid "&Bookmark This File"
#~ msgstr "Добавить &закладку на этот файл"

#~ msgctxt "@title:menu"
#~ msgid "Copy To"
#~ msgstr "Копировать в"

#~ msgctxt "@title:menu"
#~ msgid "Move To"
#~ msgstr "Переместить в"

#~ msgctxt "@title:menu"
#~ msgid "Home Folder"
#~ msgstr "Домашняя папка"

#~ msgctxt "@title:menu"
#~ msgid "Root Folder"
#~ msgstr "Корневая папка"

#~ msgctxt "@title:menu in Copy To or Move To submenu"
#~ msgid "Browse..."
#~ msgstr "Обзор..."

#~ msgctxt "@title:menu"
#~ msgid "Copy Here"
#~ msgstr "Копировать сюда"

#~ msgctxt "@title:menu"
#~ msgid "Move Here"
#~ msgstr "Переместить сюда"

#~ msgid "Share"
#~ msgstr "Совместный доступ"

#~ msgid "Icon:"
#~ msgstr "Значок:"
