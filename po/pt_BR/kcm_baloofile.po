# Translation of kcm_baloofile.po to Brazilian Portuguese
# Copyright (C) 2014-2020 This file is copyright:
# This file is distributed under the same license as the plasma-desktop package.
#
# André Marcelo Alvarenga <alvarenga@kde.org>, 2014, 2019, 2020.
# Luiz Fernando Ranghetti <elchevive@opensuse.org>, 2017, 2020, 2021, 2022.
# Thiago Masato Costa Sueto <herzenschein@gmail.com>, 2020.
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-01-26 00:59+0000\n"
"PO-Revision-Date: 2022-12-20 11:19-0300\n"
"Last-Translator: Luiz Fernando Ranghetti <elchevive@opensuse.org>\n"
"Language-Team: Portuguese <kde-i18n-pt_BR@kde.org>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 21.12.3\n"

#: package/contents/ui/main.qml:21
#, kde-format
msgid ""
"This module lets you configure the file indexer and search functionality."
msgstr ""
"Este módulo permite-lhe configurar a funcionalidade de indexação e pesquisa "
"de arquivos."

#: package/contents/ui/main.qml:58
#, kde-format
msgid ""
"This will disable file searching in KRunner and launcher menus, and remove "
"extended metadata display from all KDE applications."
msgstr ""
"Isto irá desabilitar a pesquisa de arquivos no KRunner e menus e removerá os "
"metadados estendidos exibidos em todos os aplicativos KDE."

#: package/contents/ui/main.qml:67
#, kde-format
msgid ""
"Do you want to delete the saved index data? %1 of space will be freed, but "
"if indexing is re-enabled later, the entire index will have to be re-created "
"from scratch. This may take some time, depending on how many files you have."
msgstr ""
"Deseja excluir os dados salvos do índice? %1 de espaço serão liberados, mas "
"se a indexação for reabilitada mais tarde, o índice inteiro terá que ser "
"recriado do zero. Isto pode levar algum tempo, dependendo de quantos "
"arquivos você tem."

#: package/contents/ui/main.qml:69
#, kde-format
msgid "Delete Index Data"
msgstr "Excluir dados do índice"

#: package/contents/ui/main.qml:89
#, kde-format
msgid "The system must be restarted before these changes will take effect."
msgstr ""
"O sistema deve ser reiniciado antes que estas alterações tenham efeito."

#: package/contents/ui/main.qml:93
#, kde-format
msgctxt "@action:button"
msgid "Restart"
msgstr "Reiniciar"

#: package/contents/ui/main.qml:100
#, kde-format
msgid ""
"File Search helps you quickly locate all your files based on their content."
msgstr ""
"A Pesquisa de arquivos ajuda-lhe a encontrar todos os seus arquivos com base "
"no conteúdo."

#: package/contents/ui/main.qml:107
#, kde-format
msgid "Enable File Search"
msgstr "Habilitar a pesquisa de arquivos"

#: package/contents/ui/main.qml:122
#, kde-format
msgid "Also index file content"
msgstr "Também indexar o conteúdo do arquivo"

#: package/contents/ui/main.qml:136
#, kde-format
msgid "Index hidden files and folders"
msgstr "Indexar os arquivos e pastas ocultas"

#: package/contents/ui/main.qml:160
#, kde-format
msgid "Status: %1, %2% complete"
msgstr "Status: %1, %2% concluído"

#: package/contents/ui/main.qml:165
#, kde-format
msgid "Pause Indexer"
msgstr "Pausar indexador"

#: package/contents/ui/main.qml:165
#, kde-format
msgid "Resume Indexer"
msgstr "Continuar indexador"

#: package/contents/ui/main.qml:178
#, kde-format
msgid "Currently indexing: %1"
msgstr "Indexando atualmente: %1"

#: package/contents/ui/main.qml:183
#, kde-format
msgid "Folder specific configuration:"
msgstr "Configuração específica da pasta:"

#: package/contents/ui/main.qml:210
#, kde-format
msgid "Start indexing a folder…"
msgstr "Iniciar a indexação de uma pasta..."

#: package/contents/ui/main.qml:221
#, kde-format
msgid "Stop indexing a folder…"
msgstr "Parar a indexação de uma pasta..."

#: package/contents/ui/main.qml:272
#, kde-format
msgid "Not indexed"
msgstr "Não indexada"

#: package/contents/ui/main.qml:273
#, kde-format
msgid "Indexed"
msgstr "Indexado"

#: package/contents/ui/main.qml:303
#, kde-format
msgid "Delete entry"
msgstr "Excluir registro"

#: package/contents/ui/main.qml:318
#, kde-format
msgid "Select a folder to include"
msgstr "Selecione uma pasta para incluir"

#: package/contents/ui/main.qml:318
#, kde-format
msgid "Select a folder to exclude"
msgstr "Selecione uma pasta para excluir"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "André Marcelo Alvarenga"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "alvarenga@kde.org"

#~ msgid "File Search"
#~ msgstr "Pesquisa de arquivos"

#~ msgid "Copyright 2007-2010 Sebastian Trüg"
#~ msgstr "Copyright 2007-2010 de Sebastian Trüg"

#~ msgid "Sebastian Trüg"
#~ msgstr "Sebastian Trüg"

#~ msgid "Vishesh Handa"
#~ msgstr "Vishesh Handa"

#~ msgid "Tomaz Canabrava"
#~ msgstr "Tomaz Canabrava"

#~ msgid "Add folder configuration…"
#~ msgstr "Adicionar configuração da pasta..."
