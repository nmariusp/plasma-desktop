# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# g.sora <g.sora@tiscali.it>, 2010, 2013, 2022.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-01-29 00:59+0000\n"
"PO-Revision-Date: 2022-07-08 18:29+0200\n"
"Last-Translator: giovanni <g.sora@tiscali.it>\n"
"Language-Team: Interlingua <kde-i18n-doc@kde.org>\n"
"Language: ia\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 21.12.3\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: contents/config/config.qml:13
#, kde-format
msgid "General"
msgstr "General"

#: contents/ui/ConfigGeneral.qml:27
#, kde-format
msgid "Show active application's name on Panel button"
msgstr "Monstra nomine de applicationes sur button de Pannello"

#: contents/ui/ConfigGeneral.qml:47
#, kde-format
msgid "Only icons can be shown when the Panel is vertical."
msgstr "Solmenmte icones pote esser monstrate quando le Pannello es vertical."

#: contents/ui/ConfigGeneral.qml:49
#, kde-format
msgid "Not applicable when the widget is on the Desktop."
msgstr "Non applicabile quando le widget es sur le Scriptorio."

#: contents/ui/main.qml:86
#, kde-format
msgid "Plasma Desktop"
msgstr "Plasma Desktop (Scriptorio Plasma)"

#~ msgid "Show list of opened windows"
#~ msgstr "Monstra lista de fenestras aperite"

#~ msgid "On all desktops"
#~ msgstr "Super omne scriptorios"

#~ msgid "Window List"
#~ msgstr "Lista de Fenestra"

#~ msgid "Actions"
#~ msgstr "Actiones"

#~ msgid "Unclutter Windows"
#~ msgstr "Ordina le fenestras"

#~ msgid "Cascade Windows"
#~ msgstr "Fenestras in cascada"

#, fuzzy
#~| msgid "Desktop"
#~ msgctxt "%1 is the name of the desktop"
#~ msgid "Desktop %1"
#~ msgstr "Scriptorio"

#~ msgid "Current desktop"
#~ msgstr "Currente Scriptorio"

#~ msgid "No windows"
#~ msgstr "Nulle fenestra"
