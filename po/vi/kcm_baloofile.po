# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-desktop package.
#
# Phu Hung Nguyen <phu.nguyen@kdemail.net>, 2021, 2023.
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-01-26 00:59+0000\n"
"PO-Revision-Date: 2023-01-08 16:22+0100\n"
"Last-Translator: Phu Hung Nguyen <phu.nguyen@kdemail.net>\n"
"Language-Team: Vietnamese <kde-l10n-vi@kde.org>\n"
"Language: vi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Lokalize 22.12.0\n"

#: package/contents/ui/main.qml:21
#, kde-format
msgid ""
"This module lets you configure the file indexer and search functionality."
msgstr ""
"Khối này cho phép bạn cấu hình trình lập chỉ mục tệp và chức năng tìm kiếm."

#: package/contents/ui/main.qml:58
#, kde-format
msgid ""
"This will disable file searching in KRunner and launcher menus, and remove "
"extended metadata display from all KDE applications."
msgstr ""
"Việc này sẽ tắt tính năng tìm kiếm tệp trong KRunner và các trình đơn khởi "
"chạy, và xoá bộ hiển thị siêu dữ liệu mở rộng khỏi tất cả các ứng dụng KDE."

#: package/contents/ui/main.qml:67
#, kde-format
msgid ""
"Do you want to delete the saved index data? %1 of space will be freed, but "
"if indexing is re-enabled later, the entire index will have to be re-created "
"from scratch. This may take some time, depending on how many files you have."
msgstr ""
"Bạn có muốn xoá dữ liệu chỉ mục đã lưu? %1 không gian sẽ được giải phóng, "
"nhưng sau này nếu việc lập chỉ mục được bật lại, toàn bộ chỉ mục sẽ phải "
"được tạo lại từ đầu. Việc này có thể mất thời gian, tuỳ thuộc vào số lượng "
"tệp bạn có."

#: package/contents/ui/main.qml:69
#, kde-format
msgid "Delete Index Data"
msgstr "Xoá dữ liệu chỉ mục"

#: package/contents/ui/main.qml:89
#, kde-format
msgid "The system must be restarted before these changes will take effect."
msgstr ""
"Hệ thống phải được khởi động lại trước khi các thay đổi này có thể có tác "
"dụng."

#: package/contents/ui/main.qml:93
#, kde-format
msgctxt "@action:button"
msgid "Restart"
msgstr "Khởi động lại"

#: package/contents/ui/main.qml:100
#, kde-format
msgid ""
"File Search helps you quickly locate all your files based on their content."
msgstr ""
"\"Tìm kiếm tệp\" giúp bạn nhanh chóng định vị tất cả các tệp dựa trên nội "
"dung."

#: package/contents/ui/main.qml:107
#, kde-format
msgid "Enable File Search"
msgstr "Bật \"Tìm kiếm tệp\""

#: package/contents/ui/main.qml:122
#, kde-format
msgid "Also index file content"
msgstr "Lập chỉ mục cả nội dung tệp"

#: package/contents/ui/main.qml:136
#, kde-format
msgid "Index hidden files and folders"
msgstr "Lập chỉ mục tệp và thư mục ẩn"

#: package/contents/ui/main.qml:160
#, kde-format
msgid "Status: %1, %2% complete"
msgstr "Tình trạng: %1, hoàn thành %2%"

#: package/contents/ui/main.qml:165
#, kde-format
msgid "Pause Indexer"
msgstr "Tạm dừng lập chỉ mục"

#: package/contents/ui/main.qml:165
#, kde-format
msgid "Resume Indexer"
msgstr "Tiếp tục lập chỉ mục"

#: package/contents/ui/main.qml:178
#, kde-format
msgid "Currently indexing: %1"
msgstr "Đang lập chỉ mục: %1"

#: package/contents/ui/main.qml:183
#, kde-format
msgid "Folder specific configuration:"
msgstr "Cấu hình từng thư mục:"

#: package/contents/ui/main.qml:210
#, kde-format
msgid "Start indexing a folder…"
msgstr "Bắt đầu lập chỉ mục một thư mục…"

#: package/contents/ui/main.qml:221
#, kde-format
msgid "Stop indexing a folder…"
msgstr "Dừng lập chỉ mục một thư mục…"

#: package/contents/ui/main.qml:272
#, kde-format
msgid "Not indexed"
msgstr "Không lập chỉ mục"

#: package/contents/ui/main.qml:273
#, kde-format
msgid "Indexed"
msgstr "Lập chỉ mục"

#: package/contents/ui/main.qml:303
#, kde-format
msgid "Delete entry"
msgstr "Xoá mục"

#: package/contents/ui/main.qml:318
#, kde-format
msgid "Select a folder to include"
msgstr "Chọn một thư mục để thêm vào"

#: package/contents/ui/main.qml:318
#, kde-format
msgid "Select a folder to exclude"
msgstr "Chọn một thư mục để bỏ đi"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Nguyễn Hùng Phú"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "phu.nguyen@kdemail.net"

#~ msgid "File Search"
#~ msgstr "Tìm kiếm tệp"

#~ msgid "Copyright 2007-2010 Sebastian Trüg"
#~ msgstr "Bản quyền 2007-2010 Sebastian Trüg"

#~ msgid "Sebastian Trüg"
#~ msgstr "Sebastian Trüg"

#~ msgid "Vishesh Handa"
#~ msgstr "Vishesh Handa"

#~ msgid "Tomaz Canabrava"
#~ msgstr "Tomaz Canabrava"

#~ msgid "Add folder configuration…"
#~ msgstr "Thêm cấu hình thư mục…"
